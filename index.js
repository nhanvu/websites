const express = require('express');
const path = require('path');
const app = express();

app.use('/static', express.static('./static'));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.listen(3000, () => console.log('Website is listening on port 3000!'));
